package app.striiv.com.chartproto;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.highlight.Range;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.renderer.BarChartRenderer;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Transformer;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "ChartProtoMain";

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private List<DateEntry<Double>> data;

        public PlaceholderFragment() {
            data = new ArrayList<>();
            data.add(new DateEntry<>(new GregorianCalendar(2017, 4, 1).getTime(), 4d));
            data.add(new DateEntry<>(new GregorianCalendar(2017, 4, 2).getTime(), 7d));
            data.add(new DateEntry<>(new GregorianCalendar(2017, 4, 4).getTime(), 2.5d));
            data.add(new DateEntry<>(new GregorianCalendar(2017, 4, 5).getTime(), 0d));
            data.add(new DateEntry<>(new GregorianCalendar(2017, 4, 7).getTime(), 9.22d));
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            float density = getResources().getDisplayMetrics().density;
            Log.i(TAG, "density is " + density);

            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
//            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            BarChart chart = (BarChart)rootView.findViewById(R.id.chart);
            List<BarEntry> entries = new ArrayList<>();
            Drawable icon = ContextCompat.getDrawable(getActivity(), R.drawable.goal_1);
            for(DateEntry datum: data) {
                entries.add(new BarEntry(datum.date.getDay(), (float)((double)datum.value), icon));
            }

            final BarDataSet dataSet = new BarDataSet(entries, "TODO hide this");
            dataSet.setDrawValues(false);
            dataSet.setDrawIcons(true);
            dataSet.setIconsOffset(new MPPointF(0, 22));
            dataSet.setBarBorderColor(Color.BLUE);
            dataSet.setBarBorderWidth(1f);
            dataSet.setHighLightColor(Color.MAGENTA);

            BarData barData = new BarData(dataSet);
            chart.setData(barData);
            chart.setRenderer(new BarChartRenderer(chart, chart.getAnimator(), chart.getViewPortHandler()){

                @Override
                public void drawHighlighted(Canvas c, Highlight[] indices) {
                    BarData barData = mChart.getBarData();

                    for (Highlight high : indices) {

                        IBarDataSet set = barData.getDataSetByIndex(high.getDataSetIndex());

                        if (set == null || !set.isHighlightEnabled())
                            continue;

                        BarEntry e = set.getEntryForXValue(high.getX(), high.getY());

                        if (!isInBoundsX(e, set))
                            continue;

                        Transformer trans = mChart.getTransformer(set.getAxisDependency());

                        mBarBorderPaint.setColor(set.getHighLightColor());
                        mHighlightPaint.setAlpha(set.getHighLightAlpha());

                        boolean isStack = (high.getStackIndex() >= 0  && e.isStacked()) ? true : false;

                        final float y1;
                        final float y2;

                        if (isStack) {

                            if(mChart.isHighlightFullBarEnabled()) {

                                y1 = e.getPositiveSum();
                                y2 = -e.getNegativeSum();

                            } else {

                                Range range = e.getRanges()[high.getStackIndex()];

                                y1 = range.from;
                                y2 = range.to;
                            }

                        } else {
                            y1 = e.getY();
                            y2 = 0.f;
                        }

                        prepareBarHighlight(e.getX(), y1, y2, barData.getBarWidth() / 2f, trans);

                        setHighlightDrawPos(high, mBarRect);

                        c.drawRect(mBarRect, mBarBorderPaint);
                    }
                }
            });
            RectF arbitraryBarBounds = chart.getBarBounds(entries.get(0));
            Log.i(TAG, "bar bounds width is " + barData.getBarWidth());

            YAxis yaxis = chart.getAxisLeft();
            XAxis xaxis = chart.getXAxis();
            YAxis rightAxis = chart.getAxisRight();

            // Test limit line for goals
            LimitLine ll = new LimitLine(7.5f, "7.5"); // set where the line should be drawn
            ll.setLineColor(Color.RED);
            ll.setLineWidth(4f);

            yaxis.addLimitLine(ll);

            // Test popup for... popup
            chart.setMarker(new PopUpTest(getContext()));

            // Fix view
            chart.setScaleEnabled(false);
            chart.setDragEnabled(false);
            chart.setPinchZoom(false);
            chart.setDoubleTapToZoomEnabled(false);

            // Axis Styling
            yaxis.setDrawGridLines(true);
            xaxis.setDrawGridLines(false);
            yaxis.setValueFormatter(new HourAxisFormatter());
            xaxis.setValueFormatter(new IAxisValueFormatter() {
                private final String[] weekLetters = {"S", "M", "T", "W", "T", "F", "S"};

                @Override
                public String getFormattedValue(float value, AxisBase axis) {
                    if(value < weekLetters.length) {
                        return weekLetters[(int)value];
                    }
                    return null;
                }
            });
            xaxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            rightAxis.setDrawGridLines(false);
            rightAxis.setDrawLabels(false);

            // Other styling
            chart.getLegend().setEnabled(false);

            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
            }
            return null;
        }
    }

    // TODO In real life use a re-usable structure.
    public static class DateEntry<T> {

        private final Date date;
        private final T value;

        public DateEntry(Date date, T value) {
            this.date = date;
            this.value = value;
        }
    }

    private static class PopUpTest extends MarkerView{

        private final TextView text;
        private final DecimalFormat decFormat = new DecimalFormat("#.#");

        /**
         * Constructor. Sets up the MarkerView with a custom layout resource.
         *
         * @param context
         */
        public PopUpTest(Context context) {
            super(context, R.layout.popup_test);
            text = (TextView)findViewById(R.id.popup_text);
        }

        @Override
        public void refreshContent(Entry e, Highlight highlight) {
            String output = "Total Sleep\n" + decFormat.format(e.getY()) + "m";
            text.setText(output);
            super.refreshContent(e, highlight);
        }

        private MPPointF mOffset;

        @Override
        public MPPointF getOffset() {

            if(mOffset == null) {
                // center the marker horizontally and vertically
                mOffset = new MPPointF(-(getWidth() / 2), -getHeight() / 2);
            }

            return mOffset;
        }
    }

    private static class HourAxisFormatter implements IAxisValueFormatter {

        private DecimalFormat mFormat;

        public HourAxisFormatter() {

            // format values to 1 decimal digit
            mFormat = new DecimalFormat("###,###,##.#");
        }

        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            // "value" represents the position of the label on the axis (x or y)
            return mFormat.format(value) + "h";
        }
    }
}
